!--------------------------------------------------------------------!
! $Id: func_rin.f90,v 1.3 2006/10/18 14:58:04 montero Exp $
!--------------------------------------------------------------------!

! Define the equation to calculate the outer radius

!--------------------------------------!
!                                       \
! FUNC_RIN
!                                       /
!--------------------------------------!

subroutine func_rin(xg,cs,cs2,sn,sn2,M,elM,ut_in,aom,f_out)

  implicit none

  real*8          :: xg,cs,cs2,sn,sn2,M,f_out,aom
  real*8          :: Delta_W,elM
  real*8          :: g_tt,g_ft,g_ff
  real*8          :: utx,tmp, ut_in
  intent(in) :: xg,cs,cs2,sn,sn2,M,elM,ut_in,aom
  intent(out):: f_out

  tmp     = xg**2 + aom*aom*cs2
  g_tt    = -(1.0d0 - 2.0d0*M*xg/tmp)
  g_ft    = -2.0d0*M*aom*xg*sn2/tmp
  g_ff    = sn2*(xg**2 + aom*aom + 2.0d0*M*aom*aom*xg*sn2/tmp) 

  utx     = dsqrt((g_ft**2 - g_ff*g_tt)/               &
       &    (g_ff + 2.0d0*elM*g_ft + elM**2*g_tt))


  f_out = utx - ut_in


end subroutine func_rin



