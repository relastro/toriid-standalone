
SUBROUTINE Schwarzschild_Metric_3d(xc, lapse, shift, g_cov)
	USE, INTRINSIC :: ISO_C_BINDING
	USE Parameters_Disc
	IMPLICIT NONE 
	INTEGER, PARAMETER  :: nDim = 3
	! argument list
	REAL, INTENT(IN)               :: xc(nDim)
	REAL, INTENT(OUT)              :: lapse, shift(3), g_cov(3,3)
	! Local parameters
	REAL :: x,y,z,r, r2, HH, SS

	! NonRotating (Schwarzschild) black hole in Schwarzschild Cartesian coordinates. See De Felice & Clarke Sect. 11.4
	x    = xc(1)
	y    = xc(2)
	z    = xc(3)
	!  
	r    = SQRT(x**2 + y**2 + z**2)
	r2   = r**2
	!
	HH = Mbh/r
	lapse   = SQRT(1.0-2.0*HH)
	SS = 1.0/(1.0-2.0*HH)/r**2 -1.0/r**2
	!
	shift(1) = 0.0         
	shift(2) = 0.0         
	shift(3) = 0.0        
	!
	g_cov( 1, 1:3) = (/ 1.0 + x**2*SS    , 2.0*SS*x*y,        2.0*SS*x*z       /)
	g_cov( 2, 1:3) = (/ 2.0*SS*x*y,        1.0 + y**2*SS,     2.0*SS*z*y       /)
	g_cov( 3, 1:3) = (/ 2.0*SS*x*z,        2.0*SS*z*y,        1.0 + z**2*SS    /)

END SUBROUTINE Schwarzschild_Metric_3d


SUBROUTINE KerrSchild_Metric_3d(xc, lapse, shift, g_cov)
	USE, INTRINSIC :: ISO_C_BINDING
	USE Parameters_Disc
	IMPLICIT NONE
	!
	REAL, DIMENSION(3), intent(IN) :: xc
	REAL                           :: lapse,gp,gm
	REAL,dimension(3)              :: shift
	REAL,dimension(3,3)            :: g_cov

	REAL :: r, r2
	REAL :: st, st2, delta1, rho2, sigma, zz, HH, lx, ly, lz, x, y, z, z2, SS
	REAL, PARAMETER :: P_eps = 1e-4   
	!
	! Rotating black hole in Kerr-Schild Cartesian coordinates. See De Felice & Clarke Sect. 11.4
	x    = xc(1)
	y    = xc(2)
	z    = xc(3)

	z2   = z**2
	aom2 = aom**2
	r    = SQRT( (x**2 + y**2 + z**2 - aom2)/2.0 + SQRT(((x**2 + y**2 + z**2 - aom2)/2.0)**2 + z2*aom2))

	r2   = r**2

	HH = Mbh*r2*r / (r2*r2 + aom2*z2)
	SS = 1.0 + 2.0*HH
	lx = (r*x + aom*y)/(r2 + aom2)  
	ly = (r*y - aom*x)/(r2 + aom2)
	lz = z/r  

	lapse   = 1.0/SQRT(SS)
	shift(1) = 2.0*HH/SS*lx         
	shift(2) = 2.0*HH/SS*ly         
	shift(3) = 2.0*HH/SS*lz        

	g_cov( 1, 1:3) = (/ 1.0 + 2.0*HH*lx**2, 2.0*HH*lx*ly,        2.0*HH*lx*lz       /)
	g_cov( 2, 1:3) = (/ 2.0*HH*lx*ly,       1.0 + 2.0*HH*ly**2,  2.0*HH*ly*lz       /)
	g_cov( 3, 1:3) = (/ 2.0*HH*lx*lz,       2.0*HH*ly*lz,        1.0 + 2.0*HH*lz**2 /)

	!g_contr( 1, 1:3) = (/  1.0 + 2.0*HH*ly**2 + 2.0*HH*lz**2,   -2.0*HH*lx*ly,                       -2.0*HH*lx*lz                     /)
	!g_contr( 2, 1:3) = (/ -2.0*HH*lx*ly,                         1.0 + 2.0*HH*lx**2 + 2.0*HH*lz**2,  -2.0*HH*ly*lz                     /)
	!g_contr( 3, 1:3) = (/ -2.0*HH*lx*lz,                        -2.0*HH*ly*lz,                        1.0 + 2.0*HH*lx**2 + 2.0*HH*ly**2 /)

	!g_contr = g_contr/SS

	!gp = SQRT(SS)
	!gm = 1.0/gp
END SUBROUTINE KerrSchild_Metric_3d
