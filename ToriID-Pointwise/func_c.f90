!--------------------------------------------------------------------!
! $Id: func_c.f90,v 1.4 2006/10/26 17:32:52 nagar Exp $
!--------------------------------------------------------------------!

! Define the equation to find the position of r_cusp and r_c

!--------------------------------------!
!                                       \
! FUNC_C                                 >
!                                       /
!--------------------------------------!


subroutine func_c(xg,cs,cs2,sn,sn2,elM,M,aom,f_out)

  implicit none

  real*8          :: xg,cs,cs2,sn,sn2,f_out,elM,M
  real*8          :: g_00,g_03,g_33,Omega_k,el_k
  real*8          :: tmp,aom
  intent(in) :: xg,cs,cs2,sn,sn2,elM,aom,M
  intent(out):: f_out

  tmp     = xg**2 + aom*aom*cs2
  g_00    = -(1.0d0 - 2.0d0*M*xg/tmp)
  g_03    = -2.0d0*M*aom*xg*sn2/tmp
  g_33    = sn2*(xg**2 + aom*aom + 2.0d0*M*aom*aom*xg*sn2/tmp) 


  Omega_k = dsqrt(M)/(xg**1.5 + aom*dsqrt(M))
  el_k    = - (g_03 + Omega_k*g_33)/(g_00 + Omega_k*g_03)


  f_out = el_k - elM


end subroutine func_c



