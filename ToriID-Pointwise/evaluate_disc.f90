
subroutine InitialTorusOnKerrSchild(x, Q)
	USE, INTRINSIC :: ISO_C_BINDING
	USE Parameters_Disc
	!USE Parameters, ONLY : nVar, nDim, gamma
	IMPLICIT NONE 
	INTEGER, PARAMETER  :: nDim = 3
	! argument list
	REAL, INTENT(IN)               :: x(nDim)
	REAL, INTENT(OUT)               :: Q(nVar_disc)
	! Storage for KerrSchild
	REAL                :: lapse,gp,gm
	REAL,dimension(3)   :: shift
	REAL,dimension(3,3) :: g_cov, g_contr

	IF (Mass_grav.ne.1.0) then
		PRINT *, "This code is intended to run with M=1"
		STOP
	END IF
	
	! Prepare quantities:
	CALL KerrSchild_Metric_3D(x, lapse, shift, g_cov)
	CALL evaluate_disc(x, Q, g_cov, lapse)
	
	! evaluate_disc sets hydro quantities.
	! Here, we fill up with the ADM quantities:
	
	Q(10) = lapse
	Q(11:13) = shift
	Q(14) = g_cov(1,1)
	Q(15) = g_cov(1,2)
	Q(16) = g_cov(1,3)
	Q(17) = g_cov(2,2)
	Q(18) = g_cov(2,3)
	Q(19) = g_cov(3,3)
end subroutine InitialTorusOnKerrSchild


subroutine evaluate_disc(x, Q, g_cov, lapse)
	! This subroutine sets the conserved hydro quantities associated to the disc,
	! ASSUMING you give it the metric.

	USE, INTRINSIC :: ISO_C_BINDING
	USE Parameters_Disc
	!USE Parameters, ONLY : nVar, nDim, gamma
	IMPLICIT NONE 
	INTEGER, PARAMETER :: nDim=3
	! Argument list 
	REAL, INTENT(IN)               :: x(nDim)        ! 
	REAL, INTENT(IN)               :: g_cov(3,3), lapse ! Ingoing metric at the given point
	REAL, INTENT(OUT)              :: Q(nVar_disc)  ! using nVar_disc, you can integrate this easier into GRMHD, CCZ4GRMHD, etc

	!grid loop variables
	real*8 :: xg, yg, zg            !cartesian coordinates of grid point
	real*8 :: rg, rbar, th, ph      !spherical coordinates of grid point
	real*8 :: r ! Radius

	real*8 :: sinth, costh, sinph, cosph     !trig functions of spherical coords

	real*8 :: D, S, rhosquared	 	   	       !auxiliery variables for metric computation
	real*8 :: gs_00, gs_11, gs_22, gs_03, gs_33          !Kerr metric in B-L coordinates (down)
	real*8 :: gs00, gs33, gs03                           !Kerr metric in B-L coordinates (up)

	real*8 :: u_t, ut, Omega, uphi
	real*8 :: dummy2, dummy3
	real*8 :: vr, vth, vphi
	real*8 :: v_x, v_y, v_z

	! Our output variables
	real*8 :: rho, press, eps, h, velx, vely, velz
	real*8 :: dens, sx, sy, sz, tau, w_lorentz

	real*8 :: old_lorentz

	real*8 :: vv, lor_check

	real*8 :: gxx, gxy, gxz, gyy, gyz, gzz
	real*8 :: det          !3-metric determinant 
	
	! unpack the metric
	gxx = g_cov(1,1)
	gxy = g_cov(1,2)
	gxz = g_cov(1,3)
	gyy = g_cov(2,2)
	gyz = g_cov(2,3)
	gzz = g_cov(3,3)

	!initialize variables to zero
	vphi = 0.d0
	ut = 0.d0

	! cartesian coordinates (to avoid indices)
	xg = x(1)
	yg = x(2)
	zg = x(3)
	r = SQRT(SUM(x**2))

	!spherical coordinates:schwarzschild and cilindrical radii
	rg = r*(1.0d0 + (M/(2.0d0*r)))**2
	rbar = dsqrt(xg*xg + yg*yg)           


	!spherical coordinates:th
	if (zg.eq.0.d0) then
	th = pi/2.d0
	costh = 0.d0
	sinth = 1.d0
	elseif (rbar.eq.0.d0) then
	if (zg.ge.0.d0) then
		th = 0.d0
		costh = 1.d0
		sinth = 0.d0
	end if
	if (zg.lt.0.d0) then 
		th = pi
		costh = -1.d0
		sinth = 0.d0
	end if
	else
	th = datan2(rbar,zg)		
	costh = dcos(th)
	sinth = dsin(th)
	endif


	!spherical coordinates:phi
	if (yg.eq.0.d0) then
	if (xg.gt.0.d0) then 
		ph = 0.d0
		cosph = 1.d0
		sinph = 0.d0
	end if
	if (xg.lt.0.d0) then
		ph = pi
		cosph = -1.d0
		sinph = 0.d0
	end if
	if (xg.eq.0.d0) then
		ph = 0.d0
		cosph = 1.d0
		sinph = 0.d0
	end if
	elseif (xg.eq.0.d0) then
	if (yg.gt.0.d0) then
		ph = pi/2.d0
		cosph = 0.d0
		sinph = 1.d0
	end if
	if (yg.lt.0.d0) then
		ph = 3.d0*pi/2.d0
		cosph = 0.d0
		sinph = -1.d0
	end if
	else
	ph = datan2(yg,xg)!+pi            
	cosph = dcos(ph)
	sinph = dsin(ph)
	endif

	!  lower Kerr metric in Boyer-Lidquist coordinates:
	D  	      = rg*rg - 2.d0*M*rg + aom*aom
	rhosquared = rg*rg + aom*aom*costh*costh
	S 	      = (rg*rg + aom*aom)**2.d0 - aom*aom*D*sinth*sinth

	gs_00   =  -(rhosquared**(-1.d0))*(D - aom*aom*sinth*sinth)
	gs_11   =  rhosquared/D
	gs_03   =  -(aom/rhosquared)*2*M*rg*sinth*sinth
	gs_22   =  rhosquared
	gs_33   =  S/rhosquared*sinth*sinth

	!upper metric 
	gs00 = gs_33/(-gs_03**2 + gs_00*gs_33)
	gs33 = gs_00/(-gs_03**2 + gs_00*gs_33)
	gs03 = gs_03/(gs_03**2 - gs_00*gs_33)


	! Torus structure
	dummy2     = (gs_03**2 - gs_33*gs_00) /           &
		& (gs_33 + 2.0d0*elM*gs_03 + elM**2*gs_00)

	if (dummy2.lt.0.0d0) then
	u_t    = 1.0d0
	else                    
	u_t    = dsqrt(dummy2)
	endif

	dummy3 = -1.0d0/(gs_00 + 2.0d0*Omega*gs_03 + Omega**2*gs_33)

	Omega  = -(gs_00*elM + gs_03)/(gs_03*elM + gs_33) 


	! check whether the point is indide or outside the disc.
	! Do *NOT* avoid points too close to the z axis
	if (rg.gt.r_in.and.rg.lt.r_out.and.ut_in - u_t.gt.0.0d0 &
                &.and.dummy3.gt.0.0d0) then !.and.xg.gt.M) then

		rho   = rho_scale*((gamma_mo)/(eos_k*eos_gamma)* &
			&   (ut_in/u_t - 1.0d0))**(1.0d0/(gamma_mo))

		press   = eos_k*rho**eos_gamma              
		
		eps = press/((eos_gamma - 1.d0)*rho)

		if (eps.lt.0.0d0) then
			write(*,*)'kaspita!'
			stop
		endif

		! controvariant velocities 
		vr    =  0.d0 !betar/lapse ! radial velocity: contravariant   
		vth   =  0.d0                 ! tiny ! theta velocity : contravariant
		vphi  =  Omega/lapse + gs03*lapse

		! check lorentz factor
		lor_check     = gs_11*vr*vr+gs_22*vth*vth+gs_33*vphi*vphi
		if (lor_check.gt.1.0d0) then
			write(*,*)'Something wrong with Lorentz factor!',lor_check
			write(*,*)'Omega',Omega,r,th
			stop
		endif

		! components of the 4-velocity
		ut = gs00*u_t !this is only for Schwarzschild (not Kerr)   
		uphi = Omega*(-ut)

		! treat origin
		if (rg.eq.0.d0) then
			Omega = 0.d0
		endif

		h   = 1.d0 + eps + press/rho

		!3-velocity: controvariant 
		velx = - vphi*r*sinth*sinph
		vely = + vphi*r*sinth*cosph
		velz = 0.d0

		vv = gxx*velx**2 + gyy*vely**2 + gzz*velz**2 +2.0*gxy*velx*vely +2.0*gyz*vely*velz +2.0*gxz*velx*velz
		
		if (vv.gt.1.0) then
			write(*,*) "Superluminal speed! vv=", vv
			write(*,*) "Perhaps Metric is bad: ", g_cov
			stop
		endif

		! w_lorentz as input used to be NaN. Determine on ourselves instead.
		w_lorentz = 1.d0/dsqrt(1-vv)

		v_x = gxx*velx + gxy*vely + gxz*velz	
		v_y = gxy*velx + gyy*vely + gyz*velz	
		v_z = gxz*velx + gyz*vely + gzz*velz	

		! ordinary SpatialDeterminant
		det = -gxz**2*gyy + 2*gxy*gxz*gyz - gxx*gyz**2 - gxy**2*gzz + gxx*gyy*gzz

		! conserved variables, derived from rho, press, w_lorentz
		dens = dsqrt(det)*rho*w_lorentz
		sx   = dens*h*w_lorentz*v_x
		sy   = dens*h*w_lorentz*v_y
		sz   = dens*h*w_lorentz*v_z
		tau  = dsqrt(det)*(rho*h*w_lorentz**2 - press) - dens

	else
		rho = 0.d0
		eps = 0.d0
		press = 0.d0
		h = 0.d0
		
		sx = 0.0
		sy = 0.0
		sz = 0.0
		
		dens = 0
		tau = 0

	end if !end cycle inside the disc
	
	! Set conserved variables vector with typical hydro ordering
	Q = 0
	Q(1) = dens
	Q(2) = sx
	Q(3) = sy
	Q(4) = sz
	Q(5) = tau
	! 6,7,8 = Bmag = 0
	! 9 = div cleaning psi = 0
	
	IF (.not.(dens.eq.dens)) THen
		CALL ABORT
	end if 
END SUBROUTINE evaluate_disc
