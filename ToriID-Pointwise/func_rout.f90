
subroutine func_rout(xg,cs,cs2,sn,sn2,M,ut_in,elM,aom,f_out)

! Define the equation to calculate the outer radius

  implicit none

  real*8, intent(in) 	:: xg,cs,cs2,sn,sn2,M,ut_in,elM,aom

  real*8 :: g_tt,g_ft,g_ff
  real*8 :: ut,tmp

  real*8, intent(out)	:: f_out

  tmp     = xg**2 + aom*aom*cs2
  g_tt    = -(1.0d0 - 2.0d0*M*xg/tmp)
  g_ft    = -2.0d0*M*aom*xg*sn2/tmp
  g_ff    = sn2*(xg**2 + aom*aom + 2.0d0*M*aom*aom*xg*sn2/tmp) 

  ut      = dsqrt((g_ft**2 - g_ff*g_tt)/               &
       &    (g_ff + 2.0d0*elM*g_ft + elM**2*g_tt))

  f_out = ut - ut_in

end subroutine func_rout



