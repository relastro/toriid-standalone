MODULE Parameters_Disc
    IMPLICIT NONE
    PUBLIC

    ! nVar_disc is the amount of variables written. See evaluate_disc.f90 for ordering.
    INTEGER, PARAMETER :: nVar_disc = 19
    
    ! Parameters determining the shape, etc. of the Disc. You can change them
    ! also at runtime *before* invoking compute_disc() but you should not afterwards.

    ! Constant value of angular momentum
    ! Valid ranges: 3.67 : 4.0, Within this interval you have cusp, centre and torus
    ! Default value: 3.8
    REAL :: el0 = 3.8
    
    ! How much do you want your torus to be inside the potential or above the cusp?
    ! Valid range: -1 : 1, Meaning: "-1=on the bottom of the potential, 0=at the cusp "
    ! Default value: -0.8
    REAL :: delta = -0.8

    ! Polytropic EOS
    ! Here we use geometric units
    ! Valid range for K: [0, inf]
    ! Valid range for gamma: [1, inf]
    REAL :: eos_K = 100.0
    REAL :: eos_gamma = 2.0

    ! Gravitational mass of central object (obligatory information)
    ! Here we use geometric units. You certianly want to keep M=1 here for a BH
    ! or some other value for a neutron star.
    ! In case of problems, double ensure that your Mass_grav is the correct one
    ! for the metric you pass at evaluating
    REAL :: Mass_grav = 1.0
    
    ! Mass of black hole (only used in the metrics.f90)
    REAL :: Mbh = 1.0
    
    ! Angular momentum of spacetime (used everywhere)
    REAL :: aom = 0.0

    ! Scaling factor for the density (and all derived quantities) of the torus
    ! Dimensionless, give any number > 0
    REAL :: rho_scale = 1.0

    ! Speak a bit in compute_disc()
    LOGICAL :: verbose = .true.

    ! Flag for debug output. True if you want to have debug output
    LOGICAL :: debug = .false.

    ! If you want to use debugging, you can set the number of points in x,y,z
    ! direction here.
    INTEGER :: nx = 10, ny = 10, nz = 10

    !
    ! Starting from here, the module variables are the computed by the code.
    !
    ! This are just variable combinations
    REAL :: elM,M,gamma_mo,aom2,pi

    ! This are the output numbers by compute_disc()
    ! They serve as input for evaluate_disc()
    REAL :: r_in, r_out, ut_in

END MODULE Parameters_Disc
