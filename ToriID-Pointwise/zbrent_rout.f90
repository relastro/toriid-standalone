
function zbrent_rout(x1,x2,tol,cs,cs2,sn,sn2,M,ut_in,elM,aom)

  implicit none

  real*8, intent(in) :: x1,x2,tol,cs,cs2,sn,sn2,M,ut_in,elM,aom
  real*8 :: zbrent_rout

  integer :: iter
  real*8 :: a,b,c,d,e,fa,fb,fc,p,q,t,s,tol1,xm,fo

  integer, parameter :: ITMAX=100
  real*8, parameter :: tiny=epsilon(x1)

  a=x1
  b=x2

  call func_rout(a,cs,cs2,sn,sn2,M,ut_in,elM,aom,fo)
  fa = fo

  call func_rout(b,cs,cs2,sn,sn2,M,ut_in,elM,aom,fo)
  fb = fo

  if ((fa > 0.0 .and. fb > 0.0) .or. (fa < 0.0 .and. fb < 0.0)) then
       print*,'root must be bracketed for zbrent_rout'
  endif

  c=b
  fc=fb

  do iter=1,ITMAX
     if ((fb > 0.0 .and. fc > 0.0) .or. (fb < 0.0 .and. fc < 0.0)) then
        c=a
        fc=fa
        d=b-a
        e=d
     end if
     if (dabs(fc) < dabs(fb)) then
        a=b
        b=c
        c=a
        fa=fb
        fb=fc
        fc=fa
     end if
     tol1=2.0d0*tiny*dabs(b)+0.5d0*tol
     xm=0.5d0*(c-b)
     if (dabs(xm) <= tol1 .or. fb == 0.0) then
        zbrent_rout=b
        return
     end if
     if (dabs(e) >= tol1 .and. dabs(fa) > dabs(fb)) then
        s=fb/fa
        if (a == c) then
           p=2.0d0*xm*s
           q=1.0d0-s
        else
           q=fa/fc
           t=fb/fc
           p=s*(2.0d0*xm*q*(q-t)-(b-a)*(t-1.0d0))
           q=(q-1.0d0)*(t-1.0d0)*(s-1.0d0)
        end if
        if (p > 0.0) q=-q
        p=dabs(p)
        if (2.0d0*p  <  min(3.0d0*xm*q-dabs(tol1*q),dabs(e*q))) then
           e=d
           d=p/q
        else
           d=xm
           e=d
        end if
     else
        d=xm
        e=d
     end if
     a=b
     fa=fb
     b=b+merge(d,sign(tol1,xm), dabs(d) > tol1 )
     call func_rout(b,cs,cs2,sn,sn2,M,ut_in,elM,aom,fo)
     fb = fo
  end do

  !call nrerror('zbrent_rout: exceeded maximum iterations')
  zbrent_rout=b

end function zbrent_rout





