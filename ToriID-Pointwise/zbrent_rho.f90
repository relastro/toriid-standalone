! Double precision
#define DP 8

function zbrent_rho(x1,x2,sstmp,denstmp,tol)
  !use nrtype
  !  use params_module
  INTEGER :: ITMAX
  REAL(DP) :: zbrent_rho,tol,x1,x2,EPS
!      EXTERNAL func
  PARAMETER (ITMAX=100,EPS=3.e-8)
  INTEGER :: iter
  REAL(DP) :: a,b,c,d,e,fa,fb,fc,p,q,r,s,tol1,xm

  REAL(DP) :: sstmp,denstmp,wtmp,gm

  a=x1
  b=x2

  gm   = gamma*kp/(gamma-1.d0)
  wtmp = sqrt(1.+ sstmp/(denstmp*(1.d0+gm*a**(gamma-1)))**2)
  fa   = a*wtmp - denstmp


  gm   = gamma*kp/(gamma-1.d0)
  wtmp = sqrt(1+ sstmp/(denstmp*(1.d0+gm*b**(gamma-1)))**2)
  fb   = b*wtmp - denstmp

!      write(*,*) denstmp,sstmp
!      write(*,*) x1,x2
!      write(*,*) gamma, kp

  if((fa.gt.0..and.fb.gt.0.).or.(fa.lt.0..and.fb.lt.0.)) then
     PRINT *, 'root must be bracketed for zbrent'
     STOP
  endif
      c=b
      fc=fb
      do iter=1,ITMAX
        if((fb.gt.0..and.fc.gt.0.).or.(fb.lt.0..and.fc.lt.0.))then
          c=a
          fc=fa
          d=b-a
          e=d
        endif
        if(abs(fc).lt.abs(fb)) then
          a=b
          b=c
          c=a
          fa=fb
          fb=fc
          fc=fa
        endif
        tol1=2.*EPS*abs(b)+0.5*tol
        xm=.5*(c-b)
        if(abs(xm).le.tol1 .or. fb.eq.0.)then
          zbrent_rho=b
          return
        endif
        if(abs(e).ge.tol1 .and. abs(fa).gt.abs(fb)) then
          s=fb/fa
          if(a.eq.c) then
            p=2.*xm*s
            q=1.-s
          else
            q=fa/fc
            r=fb/fc
            p=s*(2.*xm*q*(q-r)-(b-a)*(r-1.))
            q=(q-1.)*(r-1.)*(s-1.)
          endif
          if(p.gt.0.) q=-q
          p=abs(p)
          if(2.*p .lt. min(3.*xm*q-abs(tol1*q),abs(e*q))) then
            e=d
            d=p/q
          else
            d=xm
            e=d
          endif
        else
          d=xm
          e=d
        endif
        a=b
        fa=fb
        if(abs(d) .gt. tol1) then
          b=b+d
       else
          b=b+sign(tol1,xm)
       endif
       
       gm = gamma*kp/(gamma-1.d0)
       wtmp = sqrt(1+ sstmp/(denstmp*(1.d0+gm*b**(gamma-1)))**2)
       fb = b*wtmp - denstmp
       
    enddo
    PRINT *, 'zbrent exceeding maximum iterations'
    STOP
    zbrent_rho=b
    return
  end function zbrent_rho

