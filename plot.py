#!/usr/bin/env python2

# Plot the torus solution. Yay.

from pylab import *
ion()

d = genfromtxt("output.csv", names=True)

plot(d["x"], d["Dens"])
plot(d["x"], d["sy"])
plot(d["x"], d["tau"])
