Program StandaloneToriIDExample
	USE parameters_disc
	IMPLICIT NONE
	INTEGER :: i
	REAL :: x(3), Q(nVar_disc), t
	! Interpolation parameters
	REAL, PARAMETER :: xmin = -13.0, xmax = 13.0
	INTEGER, PARAMETER :: numpoints = 300
	
	! Turn off output for CSV output
	verbose = .false.

	if (verbose) then
		PRINT *, "Standalone ToriID Pointwise Evaluation Example Code"
		PRINT *, "A test with KerrSchild coordinates"
	end if

	! Change some variables in the parameters_disc.mod
	Mass_grav = 1.0 ! Keep this to 1 with KerrSchild
	el0 = 3.8
	delta = -0.7
	

	! Precompute disc properties
	CALL compute_disc()
	
	PRINT *, "x y z Dens sx sy sz tau Bx By Bz psi alpha shiftx shifty shiftz gxx gxy gxz gyy gyz gzz"
	
	! Evaluate the disc at an axis
	x = 0
	DO i=1,numpoints
		x(1) = xmin + (xmax - xmin) * i / numpoints
		!PRINT *, "Requesting initial data at ", x
		CALL InitialTorusOnKerrSchild(x, Q)
		!PRINT *, "Got back Initial data: "
		!PRINT *, Q
		
		PRINT *, x, Q
	ENDDO

END ! Program StandaloneToriIDExample

